#!/usr/bin/env python2
# This code was written by REDACTED (git{hub, lab}.com/RiderExMachina)
# Date: 2018/03/20
# Copyright under the GPL verson 2
#
# It was written to make tuneups for Macs at REDACTED easier. As 
# Mac computers have Python installed, it was simple to make a Python 
# script that goes through the temporary files and deletes them. It also 
# could sometimes save a step at the very end of the tuneup: empty trash.
#
# Any comments with a double pound sign (##) are my own comments for clarity

# Import the needed libraries
## os will let us change things around in the OS, getpass will let us guess
## the username for the user folder cleaning. Similar to %userprofile% in Windows
import os
import getpass

# Definitions
## Again, getpass is acting as %userprofile% for the later cleanup
user = getpass.getuser()
libraryFolders = ['Caches', 'Extensions', 'Input Methods', 'LaunchAgents', 'LaunchDaemons', 'ScriptingAdditions']
userFolders = ['Caches', 'Internet Plug-ins', 'LaunchAgents']

def rmFiles(directory):
	print "Removing all items in %s..." % directory
	for file in os.listdir(directory):
			## Remove all the files in the current directory
			try:
				os.remove(file)
			except OSError:
				print "Could not delete %s" % file
			else:
				print "Deleted %s" % file

def rmUserFolders():
	for folder in userFolders:
		## Change directories to /Users/$USER/Library/$FOLDER and set that directory as the base
		os.chdir('/Users/%s/Library/%s' % (user, folder))
		directory = os.getcwd()
		rmFiles(directory)
	
	print "Done!"

def rmLibraryFolders():
	for folder in libraryFolders:
		## Change directories to /Libraries/$FOLDER and set that directory as the base
		os.chdir('/Library/%s' % folder)
		directory = os.getcwd()
		rmFiles(directory)
	
	print "Done!"

def getRoot():
	current_script = os.path.realpath(__file__)
	os.system('sudo -S python %s' % current_script)


print "Clearing temp files"
if user != 'root':
	rmUserFolders()
	getRoot()
	user = getpass.getuser()
	if user != 'root':
		exit()
if user == 'root':
	rmLibraryFolders()
	exit()
