#!/usr/bin/env python3
# This code is written by REDACTED (git{hub, lab}.com/RiderExMachina)
# Date: 2018/12/21
# Copyright under GPL version 2

# It was created as an exercise in Regex, but also for another use:
# I was once put into a position where I needed to look up the owner
# of a MAC Address. So I wrote this in case I need it in the future.

# The Mac Address information comes from 
# https://gist.github.com/aallan/b4bb86db86079509e6159810ae9bd3e4/raw/846ae1b646ab0f4d646af9115e47365f4118e5f6/mac-vendor.txt
# and this script may include a way to download that file in the future,
# but I'm still on the fence.

# Any comments with a double pound sign (##) are my own comments for clarity

# Import the needed libraries
## io allows us to import the file using utf-8. Using the built-in open throws an error
## re imports Regex for easy searching
import io
import re

def search(macAddress):
	found = False
	macLookUp = re.compile(macAddress, re.IGNORECASE)

	with io.open("mac-vendor.txt", 'r', encoding="utf-8") as macFile:
		macs = macFile.readlines()
		
		for i in macs:
			result = macLookUp.search(i)
			
			if result != None:
				found =True
				print("\nMAC Address found!\n")
				print("ADDRESS      OWNER ")
				print(i)
				break

	if found == False:
		print("\nMAC Address not found. Try another?")
		ask()

def macCheck(macAddress):
	check = [ "0", "1", "2", "3", "4", "5", "6", "7", "8","9", 
			"a", "b", "c", "d", "e", "f" ]
	for i in macAddress:
		if i.lower() not in check:
			print("\nInvalid character!")
			ask()
	
	search(macAddress)

def ask():
	print("\n\nWhat MAC Address are you looking up?")

	macAddress = input("(Just the first 6 characters, please)\n\n> ")
	if len(macAddress) == 6 or len(macAddress) == 12:
		if len(macAddress) == 12:
			macAddress = macAddress[0:6]
			macCheck(macAddress) 
		else:
			macCheck(macAddress)
	elif macAddress == "quit" or macAddress == "exit":
		exit()
	else:
		print("\nThat's not acceptable!")
		ask()

def main():
	print("+===========================================+")
	print("|                                           |")
	print("|   Welcome to the MAC Address lookup DB.   |")
	print("|                                           |")
	print("+===========================================+")

	ask()

main()