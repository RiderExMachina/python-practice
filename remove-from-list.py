#!/usr/bin/env python3
# This code was written by David Seward (git{hub, lab}.com/RiderExMachina)
# 
# This was inspired by an episode of Brain Games (04x09), where contestants
# competed to guess a picture that was revealed piece by piece by 
# removing squares at random. I wanted to see if I could do the same thing 
# in Python.
#
# Turns out, I could!

import random
import sys

squares = list(range(1, 51))
with open("removed.txt", "a") as log:
	while squares != []:
		#print(squares)

		rm = random.randint(0, len(squares)-1)
		print("Removing element " + str(rm) + " ("+ str(squares[rm]) +")")
		log.write("Removed square " + str(squares[rm]) + "\n")

		squares.pop(rm)

print("Finished!\n\n")