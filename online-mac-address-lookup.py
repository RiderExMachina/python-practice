#! /usr/bin/env python3
# This code is written by REDACTED (git{hub, lab}.com/RiderExMachina) Copyright under GPL version 2
# Date: 2018/12/21
# Any comments with a double pound sign (##) are my own comments for clarity

# Import the needed libraries
## re imports Regex for easy searching
## time is for waiting--makes the script feel organic and also keeps us from bogging down the online search
## requests allows us to grab the information from an online website. It may need to be installed using `pip3 install requests`


import re
import time
import requests

wait = time.sleep(3)

def onlineSearch(macAddress):
	print("Searching online database...")
	wait
	macAddress = str(macAddress)
	link = ("https://api.macvendors.com/")
	result = requests.get(link + macAddress)

	if result.status_code == 200:
		owner = result.text

		print("\nMAC Address found!\n")
		print("ADDRESS | OWNER ")
		print(macAddress.upper() + "  | " + owner)
		ask()
		
	else:
		print("\nMAC Address not found. Try another?")
		ask()

def macCheck(macAddress):
	# Check for non-numeric characters to remove
	badCharacters = [ ".", ":", "-", ";" ]
	# Check for valid hex characters
	check = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" ]
	
	# Don't need to check if we know it doesn't have non-numeric characters in it
	if len(macAddress) != 6: 
		for i in badCharacters:
			# Remove anything from the badCharacters array
			macAddress = macAddress.replace(i, "")

	for i in macAddress:
		if i.lower() not in check:
			# If there is a non-hex character we're obviously not going to find anything
			print("\nInvalid character!")
			ask()
	
	onlineSearch(macAddress)

def ask():
	print("\n\nWhat MAC Address are you looking up?")

	macAddress = input("\n> ")
  
  # Allow full mac addresses including the most common 00(:)00(-)00 and 0000.00 formats.
	allowed = [ 6, 12, 14, 17 ]
	leave = ["quit", "exit", "leave"]
	if len(macAddress) in allowed or if macAddress.lower() in leave:
		if len(macAddress) == 12:
			# Full address was entered without any non-numeric characters
			# Just take the first 6 characters
			macAddress = macAddress[0:6] 
		elif len(macAddress) == 14:
			# Address was entered in 0000.0000.0000 format
			macAddress = macAddress[0:7]
		elif len(macAddress) == 17:
			# Address was entered as 00-00:00|00;00 format
			macAddress = macAddress[0:8]
		elif macAddress.lower() in leave:
			# Allow easy exiting
			exit()

		macCheck(macAddress)
		
	else:
		print("\nThat's not acceptable!")
		ask()

def main():
	print("\n+===========================================+")
	print("|                                           |")
	print("|   Welcome to the MAC Address lookup DB.   |")
	print("|                                           |")
	print("+===========================================+")

	ask()

main()